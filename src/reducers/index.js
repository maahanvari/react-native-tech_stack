import { combineReducers } from 'redux';
import LibraryReducer from './LibraryReducer';
import SelectioinReducer from './SelectionReducer';

export default combineReducers({
    libraries: LibraryReducer,
    selectedLibraryId: SelectioinReducer
});

// console.log(store.getState());
//{ librarie: '' [ { id: , title: '', description: ''}]}
