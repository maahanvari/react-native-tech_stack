// it's responsible to show a single library 
import React, { Component } from 'react';
import { View,
         Text,
         TouchableWithoutFeedback,
         UIManager,
         Platform,
         LayoutAnimation 
} from 'react-native';
import { connect } from 'react-redux';
import { CardSection } from './common';
import * as actions from '../actions'; 
// give me every thing that was exported from actrion file and assing it to variable action

class ListItem extends Component {

    constructor() {
        super();
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }
    componentWillUpdate() {
        LayoutAnimation.spring();
    }

    renderDescription() {
        const { library, expanded } = this.props;

        if (expanded) {
            return (
                <CardSection>
                    <Text style={{ flex: 1 }}> 
                         {library.description} 
                    </Text>
                </CardSection>
            );
        }
    }

    render() {
        const { titleStyle } = styles;
        const { id, title } = this.props.library;

       return (
           <TouchableWithoutFeedback onPress={() => this.props.selectLibrary(id)}>
                <View>
                    <CardSection>
                        <Text style={titleStyle}> 
                            {title}
                        </Text>
                    </CardSection>
                    {this.renderDescription()}
                </View>
           </TouchableWithoutFeedback>
       );
    }
}

const styles = {
    titleStyle: {
        fontSize: 18,
        paddingLeft: 15
    }
};

const mapStateToProps = (state, ownProps) => {
    const expanded = state.selectedLibraryId === ownProps.library.id;
            // expandedcan be true or false 
    return { expanded };
        // because my key and value are identical, I can write expand insteed expand: expand
};

export default connect(mapStateToProps, actions)(ListItem);

// we pass the function map state to the props as the first argument
// fisrt argument must be the map state to props function
// Second argument is to bind action creator to this component
// if I don't want to map state to props I haveto pass null as the first argument 
