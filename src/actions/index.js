//Action Creator is a javaScript function that create an action. 
export const selectLibrary = (librraryId) => {
    return {
        type: 'select_library',
        payload: librraryId
    };
};
